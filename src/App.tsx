import ListGroup from "./components/ListGroup";

const App = () => {
  let items = [
    "Pretoria",
    "Alger",
    "Bamako",
    "Yaounde",
    "Abudja",
    "Niamey",
    "Cairo",
    "Khartoum",
    "Ouagadougou",
  ];
  return (
    <div>
      <ListGroup items={items} heading="Cities" />
    </div>
  );
};
export default App;
